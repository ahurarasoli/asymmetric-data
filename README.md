# asymmetric Data  


Our goal is for everyone to have access to the free internet. In this project, we used a Python script to manipulate the input data so that those who monitor the servers do not know that VPN is running on these servers.

What will happen if these people (the entity that provides the server) find out that you created a VPN on their servers?

-<b> ‌In the first stage, they take the server from you and cut off your access to it, and they don't pay any money for this. <b>

-<b> In the second stage, there is a possibility of arrest and going to jail. <b>

<img src="https://gitlab.com/ahurarasoli/asymmetric-data/-/raw/main/image/Asymmetric_data_image.png">
